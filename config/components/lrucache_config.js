let server_options = require('./server_options');

let options = {
    // set max size of cache here.
    max: server_options.cache_size,
    // length function calculates the length of one object
    // used when comparing against max
    length: function (n, key) {
        return JSON.stringify(n).length;
    },
    dispose: (n)=>{
        console.log('removing ' + n);
    },

    // this sets the max age of an object in cache,
    // this is in ms
    // let the 1000 * stay to put values in second
    maxAge: 1000 * server_options.freshness
};

module.exports = options;