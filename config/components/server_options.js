'use strict';

const server_options = {
    // this is the port i am listening on
    port: 8000,
    // this is my target server and port
    server: "www.urbandictionary.com",
    server_port: 80,
    // this is my delay in ms.
    delay: 10000,
    cache_size: 10000,
    // this value is in seconds
    freshness: 1000
};

module.exports = server_options;