'use strict';

let server_options = require("../../config/components/server_options");
let http = require('http');
let cache = require('../../models/cache/lrucache');

let http_post = (url, headers, data, req, res, respond) => {
    let options = {
        host: server_options.server,
        path: url,
        port: server_options.server_port,
        method: 'POST',
        headers: headers
    };
    let callback = (response) => {
        //response.pipe(res);
        const returned_data = [];
        response.on('data', function (data) {
            returned_data.push(data);
        });
        response.on('end', function () {
            let resp = {
                statusCode: response.statusCode,
                statusMessage: response.statusMessage,
                headers: response.headers,
                response: Buffer.concat(returned_data)
            };
            cache.set(JSON.stringify({'method':'post','url':req.url,'data':data}), resp);
            respond(req, res, resp);
        });
    };
    let post = http.request(options, callback).on('error', function (error) {
        let resp = {
            statusCode: 404,
            statusMessage: error,
            response: ''+error,
            headers: {'content-type':'text/html'}
        };
        respond(req, res, resp);
    });
    post.write(data);
    post.end();
};
module.exports = http_post;