'use strict';
let cache = require('./lrucache');
let q_string = require('querystring');

let cache_api_call = (req, res, respond) =>{
    console.log('Cache API Called');
    let statusCode = 200;
    let statusMessage = 'Ok';
    let response = '';
    let headers = {'Content-Type':'text/html'};
    switch(req.method){
        case "GET":{
            console.log('Cache API GET Called');
            let query = q_string.parse(req.url.split('/admin/cache?')[1]);
            let cache_entry = cache.get(query.key);
            if(cache_entry !== undefined){
                console.log('cache entry found');
                if(cache_entry.response !== undefined){
                    response = cache_entry.response;
                }else{
                    response = cache_entry;
                }
                if(cache_entry.headers !== undefined){
                    headers = cache_entry.headers;
                }
                if(cache.statusCode !== undefined){
                    statusCode = cache.statusCode;
                }
                if(cache.statusMessage !== undefined){
                    statusCode = cache.statusMessage;
                }
            }else{
                statusCode = 404;
                statusMessage = 'Not Found';
                response = '404 - not found';
            }
            break;
        }
        case "POST":{
            console.log('Cache API POST Called');
            if(req.url.includes('reset')){
                cache.reset();
                response = 'Cache reset';
            }
            break;
        }
        case "PUT":{
            console.log('Cache API PUT Called');
            if(req.url.includes('/admin/cache?')){
                let query = q_string.parse(req.url.split('/admin/cache?')[1]);
                cache.set(query.key, query.value);
                response = 'added to cache';
            }else{
                statusCode = 404;
                statusMessage = 'Wrong use of API';
                response = 'Wrong use of API';
            }
            break;
        }
        case "DELETE":{
            console.log('Cache API DELETE Called');
            if(req.url.includes('/admin/cache?')) {
                let query = q_string.parse(req.url.split('/admin/cache?')[1]);
                let key = query.key;
                response = 'deleted from cache';
                let deleted = cache.get(key);
                if(deleted !== undefined){
                    console.log('DELETE SUCCESSFUL');
                    cache.del(key);
                    statusCode = 200;
                    statusMessage = 'deleted ' + key;
                    response = 'deleted ' + key;
                }else{
                    statusCode = 404;
                    statusMessage = 'Wrong use of API';
                    response = 'Wrong use of API';
                }
            }else{
                statusCode = 404;
                statusMessage = 'Wrong use of API';
                response = 'Wrong use of API';
            }
            break;
        }
    }
    console.log(cache);
    respond(req, res, {
        statusCode: statusCode,
        statusMessage: statusMessage,
        response: response,
        headers: headers
    }, '');
};

module.exports = cache_api_call;