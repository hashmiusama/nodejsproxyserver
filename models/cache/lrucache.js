'use strict';

let lru_cache = require("lru-cache") ;
let options = require('../../config/components/lrucache_config');
let cache = lru_cache(options);

module.exports = cache;