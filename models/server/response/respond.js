'use strict';

let server_options = require('./../../../config/components/server_options');

// the purpose of the method is to simply get data object and write back to the response.
// this function will be given two parameters
// the response code and response message and the data/payload as second parameter.
// and response as the first parameter

let respond = function(req, res, data, type){
        console.log('############################################################################');
        console.log(res.cook);
        console.log('############################################################################');
        //res.setHeader('Set-Cookie', res.mycookie.val);
        //data.headers['Set-Cookie'] = res.myCount.value+';'+res.myTime.value;
        //console.log(data.statusMessage, data.statusCode, data.headers);
        if(data.headers){
            if(data.headers['content-encoding']){
                delete data.headers['content-encoding'];
            }
        }
        res.writeHead(data.statusCode, [['Set-Cookie', res.cook.time ], ['Set-Cookie', res.cook.count]], data.headers);
        if(type === ''){
            res.end(data.response);
        }else{
            // EXTRA CREDIT IMPLEMENTATION HERE
            let rep = data.response.toString('utf-8');
            // replacing servername anywhere in code with localhost:8000
            let resp = rep.replaceAll(server_options.server,'localhost:8000');
          //  console.log(rep);
            res.write(resp, 'utf8');
            res.end();
        }
};

module.exports = respond;