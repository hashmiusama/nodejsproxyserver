'use strict';

let request_type_checker = (req, res, respond)=>{
    let emitter = require('./../../../emit_handlers/emithandler');
    if(req.method === "GET"){
        let get = require('../request/handlers/get');
        emitter.emit('type_checker__get', req, res, respond);
    }
    if(req.method === 'POST'){
        let post = require('../request/handlers/post');
        emitter.emit('type_checker__post', req, res, respond);
    }
};

module.exports = request_type_checker;