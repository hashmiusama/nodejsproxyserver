'use strict';

// step 1: partially processing headers here.
// checking what kind of request it is

let emitter = require('./../../../emit_handlers/emithandler');
let respond = require('./../../server/response/respond');

let request_handler = function(req, res){
    if(req.url.includes('/admin/')){
        // admin api call will not require to be counted by counter
        emitter.emit('handler__cache_api', req, res, respond);
    }else {
        emitter.emit('handler__request_count', req, res, respond);
    }
};

module.exports = request_handler;