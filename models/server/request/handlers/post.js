'use strict';

// step 2: processing the query string
// (post data in this case) here.
let cache = require('../../../../models/cache/lrucache');

let post_handler = function(req, res, respond){
    let emitter = require('./../../../../emit_handlers/emithandler');
    let input = '';
    req.on('data', function (data) {
        input += data;
    });
    req.on('end', function () {
        let cached_response = cache.get(JSON.stringify({'method':'post','url':req.url,'data':input}));
        if(cached_response !== undefined){
            console.log('getting from cache');
            respond(req, res, cached_response);
        }else {
            emitter.emit('post__http_post', req.url, req.headers, input, req, res, respond);
        }
    });
};
module.exports = post_handler;